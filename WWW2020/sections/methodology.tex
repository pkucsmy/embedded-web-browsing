%!TEX root = ../WWW2019-WebView.tex			

%Although it is common that Android apps embed WebViews to provide functionality, little is known on the characteristics and performance of embedded Web browsing. To this end, we conduct a measurement study to demystify the embedded Web browsing on Android.

%In this section, we describe the setup of our measurement study.

Given the prevalence of embedded Web pages, we next study the performance and characteristics of embedded Web pages. There are several developer tools available to measure the performance of traditional browsers. Unfortunately, measuring the performance of embedded Web pages is more challenging.

 First, embedded Web browsing is triggered only if the user reaches the specific activity in the app through a series of UI interactions. These UI interactions are different for different apps, and cannot be obtained {\em a priori}. Second, the developer tools designed to analyze Web performance cannot be directly used to analyze embedded browsing. Although the Chrome browser provides a GUI-based tool to remotely debug embedded pages, they cannot be accessed programmatically.

\subsection{DroidMeter Architecture}

\begin{figure}
    \includegraphics[width=0.45\textwidth]{new_figures/tool}
    \caption{Architecture of DroidMeter for searching and measuring embedded Web pages.}\label{fig:tool}
\end{figure}

We address these two challenges by building an automation tool DroidMeter\footnote{We will release DroidMeter upon publication.} to analyze the performance of embedded pages. Figure~\ref{fig:tool} shows the architecture of DroidMeter, which consists of three components: {\em explorer, replayer}, and {\em page collector}.

Given an app under analysis, the explorer launches the app and traverse to app pages. During the traversal, the explorer builds a state machine of page transition. If the tool lands on an activity that embeds a Web page (for example, pages P4 and P5 shown in dashed line), the tool generates a replay script. This replay script records the set of steps from the beginning of the app to the activity with the embedded Web page. Using the script, the replayer can trigger the embedded Web page. Meanwhile, the page collector retrieves performance metrics about the embedded Web pages. We give the details of each component as follows.

%As a result, it is difficult to trigger the browsing behavior. On the other hand, since the Web browsing occurs inside apps, both the Web logic and the app's native logic are working at the same time. For example, when the WebView is requesting resources to display contents, the background thread of the app may contact with the server checking the update status. So the captured network traces may contain both the Web requests as well as the update checking requests, which are not parts of the embedded Web browsing.  As a result, it is hard to gather the measurement data of embedded Web browsing.

%Chrome remote debugging protocol provides APIs to control and monitor the Chrome browsers on desktop computers, but these APIs are not explicitly provided for the Android Chrome browser and WebViews in apps. As a result, to gather information of embedded Web pages, one can only manually copy and paste, and sometimes even has to write it down when the data cannot be copied.

\noindent \textbf{Explorer:}
The explorer is a modified version of the Monkey tool~\cite{Monkey} provided by the Android SDK. The Monkey tool is an automation tool that can traverse through the different activities of the app but randomly clicking and typing on the UI of the app. Similar to the Monkey tool, the explorer launches the app and performs a depth-first search to traverse the app activities. Searching essentially involves clicking on various UI elements to either trigger a change in UI or trigger a navigation to a different activity. The explorer performs dynamic analysis to identify embedded Web pages in an activity. Specifically the explorer parses the UI structure of each activity to identify if the activity contains an embedded Web page.

There are two key problems with existing automation tools such as the Monkey tool~\cite{Monkey}. The first is coverage and the second is repeated traversal to the same activity. The DroidMeter explorer avoid both by (a) maintaining a state machine and (b) using the UI structure of each activity to distinguish between the activities. DroidMeter build a page transition model. The model is a state machine where each node represents an app page and the edge represents the page transition via events.

DroidMeter is able to differentiate between the activities using the structure of the UI tree~\cite{uitree}. The UI tree consists of UI elements of an app page similar to a DOM tree of Web pages. Since the structure does not change significantly during replay, DroidMeter is able to faithfully reach the target embedded Web pages.

\noindent \textbf{Replayer:}
At the end of the exploration, DroidMeter generates a script to reach all activities with an embedded Web page. For example, in Figure~\ref{fig:tool} page P4 is an app page with embedded Web page, and the replay script of P4 is to first trigger event e1 on page P1 and then trigger event e3 on page P2. To open the page, the replayer component triggers each event according to the script. The state machine we built as part of the explorer is used to locate the UI elements that will trigger each transformation.

\noindent \textbf{Page collector.}
During the replay, the page collector retrieves information about the loading procedure of both the embedded Web page and the Android native activity.
To retrieve information of embedded Web pages, we port the chrome debugging tools, specifically to expose debug APIs. The debugging port on the mobile device is forwarded to a local port from which we  obtain information about resources downloaded, timing information, and information about HTTP request/response.
%We use the APIs in the network domain of Chrome remote debugging protocol~\cite{networkdomain}. Network domain provides APIs to  track network activities of the Web page. It exposes information about http, file, data and other requests and responses, their headers, bodies, timing, etc.
%We organize the retrieved information based on the HAR (HTTP ARchive) specification~\cite{har} for analysis.

\subsection{Measuring Page Load Time}

In order to compare the performance of mobile apps with and without embedded Web pages, DroidMeter measures the page load time of both embedded Web pages and native apps. The page collector ports the chrome debugging tools that provides information about the page load time.

To measure the equivalent of the page load time of mobile apps, we need to measure the time that elapses between the beginning of the activity and the time when the callback method onCreate() finishes executing. We use the Xposed Framework to inject some code before and after the  onCreate() method to perform this measurement. To get the timestamp of when the application activity launches, we use the method \texttt{reportFullyDrawn()}. This method is designed for debugging and optimizing purpose and does not affect the behavior of the activity. %But in each activity, this method can be called only once. Repeated calls to this method will lead to all the calls except the first one being ignored. We inject this method call into activities just before the activity's onCreate() method is called, alongside with codes which will print the current timestamp into logcat. With these two timing data, we can calculate the timestamp of the very beginning of an activity, then we can know the activity's timing information.

%\subsection{Porting Web developer tools to apps}

%Measuring the characteristics and performance of embedded pages is not only challenging because of the lack of automation tools, but also because existing Web browsing tools do not work from within the app. Instead, we port the chrome debugging tools for embedded Web pages, specifically to expose debug APIs. %The debugging port on the mobile device is forwarded to a local port. Therefore, the trace collector can use the local port to retrieve needed information of the Web pages and browser behaviors.
%Specifically, we obtain information about resources downloaded, track timing information, as well as HTTP request/response.
%We use the APIs in the network domain of Chrome remote debugging protocol~\cite{networkdomain}. Network domain provides APIs to  track network activities of the Web page. It exposes information about http, file, data and other requests and responses, their headers, bodies, timing, etc.
%We organize the retrieved information based on the HAR (HTTP ARchive) specification~\cite{har} for analysis.

\subsection{Measurement Setup}

To analyze the loading procedure of Web pages in embedded Web browser (i.e. WebView), we focus on the top 100 apps, from both Google Play and Wandoujia.
We install each app on a Nexus 5 smartphone (2.3GHz, 2GB RAM, and 16GB ROM) with Android 7.0. We run DroidMeter to record the set of UI inputs to trigger an embedded Web page load. The search and record process takes about 40 minutes per application. We replay the UI inputs to trigger the embedded Web page load, and use our analysis tool to analyze the performance.

\begin{table}[t!]
\caption{Apps and embedded Web pages for measurement.}\label{table:measurement:apps}
\begin{tabular}{lrr}
\hline
            & \multicolumn{1}{l}{\# apps} & \multicolumn{1}{l}{\# embedded Web pages} \\ \hline
Google Play & 22                          & 39                                        \\
Wandoujia   & 77                          & 406                                       \\ \hline
\end{tabular}
\end{table}

We were able to find 39 embedded Web pages from 22 Google Play apps, and 406 embedded Web pages from 77 Wandoujia apps, as shown in Table~\ref{table:measurement:apps}. As for the remaining apps, our exploration tool did not reach an activity with the embedded Web page.

We use this measurement set up to study the characteristics (\S\ref{sec:characteristics}) and performance (\S\ref{sec:performance}) of embedded Web pages.


%For each page reported to have embed Web pages, we replay the generated script to open the page twice and retrieve the network activities. We open each page twice in order to analyze the cache efficiency in the page loading procedure.

%We focus on some basic features of embedded Web pgaes such as its loading procedure and resources. To describe the loading procedure, we adopt the HAR (HTTP ARchive) file which contains overall information of the requests and responses in page loading such as the request time, the MIME type and the response content. For each page reported to embed WebView by our tool, we replay the generated script to open the page twice and retrieve the HAR file and some other information of the page automatically. We open each page twice in order to analyze the cache efficiency in the page loading procedure.

%From the data collected from massive apps, we can obtain much knowledge about WebViews. By analyzing the resources of WebView, we can know where and for what purpose are WebViews usually used. By analyzing the HAR file and difference between the raw HTML and the rendered DOM, we can know how JavaScripts influences the Web page content and loading. By analyzing the difference between the two HAR files we can know the cache efficiency of WebViews.


% TODO: use new data
% their embedded Web pages may reside deeply inside and reaching those pages needs complex input events which cannot be handled by our tool.

%After collecting the webview data, we use an another program to analyze it. From HAR file we get the URL of the page. We classify the requests into different catogeries and calculate the total number and total trasferred size(content may be compressed) of requests in each catogery. We calculate the number of each tag in the rendered DOM of the page. By comparing the content in the first and second HAR file and considering the cache status in the second HAR file, we know the cache effieciency of the page. We write those information into a CSV file.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
%\begin{table*}[t!]
%\centering
%\caption{My caption}
%\label{my-label}
%\begin{tabular}{|l|l|}
%\hline
%\multirow{4}{*}{requests and responses} & total number of requests                                           \\ \cline{2-2}
%                                        & total number of requests in each categories                        \\ \cline{2-2}
%                                        & total size of response body.                                       \\ \cline{2-2}
%                                        & total size of response body in each categories                     \\ \hline
%\multirow{4}{*}{HTML}                   & the primary HTML                                                   \\ \cline{2-2}
%                                        & the rendered DOM                                                   \\ \cline{2-2}
%                                        & total tags in the rendered DOM                                     \\ \cline{2-2}
%                                        & total $<$script$>$ tags in the rendered DOM         \\ \hline
%\multirow{3}{*}{macro usage}            & the name of the activity that displays webview                     \\ \cline{2-2}
%                                        & the content of the webview                                         \\ \cline{2-2}
%                                        & the proportion that webview takes up in screen                     \\ \hline
%\multirow{3}{*}{performance}            & the cache effieciency (how many items should be served from cache) \\ \cline{2-2}
%                                        & the time needed to load the page                                   \\ \cline{2-2}
%                                        & the speed index of the page                                        \\ \hline
%\end{tabular}
%\end{table*}

%The program has a simple structure. The main module uses two submodules to resolve the HAR file and HTML file respectively. After collecting needed information, the main module write it into CSV file.
