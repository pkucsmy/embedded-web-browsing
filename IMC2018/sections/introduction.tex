%!TEX root = ../IMC-WebView.tex


Native mobile applications (a.k.a., apps) and mobile Web pages are two of the most popular ways for users to access mobile content~\cite{Serrano:Software2013,Ma:TMC2018}. While these two medium of access may seem distinct, it is becoming increasingly common for mobile apps to embed a Web page within their apps.

We call Web pages embedded in mobile apps, {\em embedded pages}. When users navigate to the embedded page in the app, a Web page opens from within the app, and users can interact with the page as they usually do on their browser. Figure~\ref{fig:example} shows an example of embedded pages for the app {\em All Football}. Interestingly, part of the screen (indicated by the red dotted lines) is a Web page loaded on the embedded browser, while the rest of the screen is rendered using the native app. Mobile apps embed Web pages for various reasons including portability across platforms, re-using third-party content easily, and for uniform interface for the users.

The problem is that little is known about the characteristics and performance of embedded Web browsing. Much of the work in Web performance focus on Web pages loaded when the user types a URL on their browser, that we call {\em traditional browsing} for ease of exposition. The result is that, it is not clear if the characteristics and bottlenecks of embedded browsing is similar to that of traditional browsing. This in-turn means that, optimizations designed for mobile Web browsing may not apply to embedded browsing.

To this end, we design a set of tools\footnote{The tools designed in this study can be found at \url{http://sei.pku.edu.cn/~mayun11/webview/}.} specifically to study embedded Web pages and answer the following questions: (1) How prevalent are embedded Web pages among mobile apps?, and (2) What are the characteristics and load performance of embedded Web pages vis-a-vis traditional Web pages?


%
%On mobile devices, apart from the conventional Web browsing via browsers, there is another fashion of Web browsing which mobile users may be unaware of but is prevalent on mobile devices: Web browsing inside mobile apps. Mobile OSes provide browser components, such as WebView on Android and UIWebView on iOS, enabling app developers to implement functionalities via Web technologies (HTML, CSS, and JavaScript). Therefore, when users visit the pages that embed browser components, they are actually browsing the Web. We call such a kind of Web browsing as \emph{embedded Web browsing}.

%Mobile devices have surpassed desktop computers to become the dominant entrance to the Internet~\cite{mobiledesktop}. It is reported that mobile users prefer to use mobile apps rather than Web browsers to access the Internet because mobile apps have better user experience than mobile Web browsing~\cite{Liu:TSE2018}. While directly surfing the Web through browsers is losing attractions from mobile users, there is another form of Web browsing that users are unaware of but is prevalent on mobile devices: Web browsing inside mobile apps. Mobile OSes provide browser components, such as WebView on Android and UIWebView on iOS, enabling apps to implement functionalities via Web technologies (HTML, CSS, and JavaScript). Therefore, when users use mobile apps that embed browser components, they are actually browsing the Web. We call such a kind of Web browsing as \emph{embedded Web browsing}.


As a first step, we analyze the top 20,000 Android apps from Wandoujia~\cite{wandoujia}, the leading Android app store in China.  We mark a page as an embedded Web page if it embeds a browser in its UI layout. Mobile OSes enable developers to embed Web pages using browser components, such as WebView on Android~\cite{WebView} and UIWebView on iOS~\cite{UIWebView}. We find that 9,352 (47\%) of our 20,000 apps embed the WebView browser component. Interestingly, the more the popularity of the app, the higher the chance that the app embeds a Web page. Among the top 1000 of the apps, 80\% of the apps embed a browser component.

Unfortunately, characterizing the performance of the embedded Web page loads is not straightforward. Traditional Web page load performance can be studied at scale by loading the Web page URL on the browser programmatically. However, loading the embedded Web page requires that we navigate to the application page that contains the embedded Web page. Triggering this embedded page load involves navigating through multiple UI screens on the app, requiring user inputs that are not known {\em a-priori}. Further, embedded browsers do not support APIs for developer tools that are available in most modern browsers, making performance analysis more challenging.

We use a modified version of the Monkey tool~\cite{Monkey} to explore the mobile app to trigger an embedded page load. Our tool, which extends our work DroidWalker~\cite{DroidWalker}, explores the app by clicking on UI elements to navigate to various pages. Different from the Monkey tool, DroidWalker is stateful and avoids repeating the exploration space. When the tool triggers an embedded Web page, we record the UI path. Afterwards, we replay the UI inputs to trigger the embedded Web page load.

Since APIs for developer tools are not available to analyze embedded Web page performance, we port existing tools. Specifically, we re-implement the Chrome remote debugging~\cite{ChromeRemote} and expose the APIs. This porting allows us to collect performance metrics about the embedded page load, including load times and information about Web resources that are loaded.

%Different from measuring the conventional Web browsing in browsers, measuring embedded Web browsing faces two challenges. On one hand, it is difficult to trigger the browsing behavior due to the difference of interaction manners between conventional and embedded Web browsing. Rather than using a URL to directly open a Web page inside browsers in the case of conventional Web browsing, embedded Web browsing occurs only by performing actions on the app to reach those app pages that embed browser components. On the other hand, it is hard to purely gather the information of embedded Web browsing because the ``embedded'' nature mixes the behaviors of browsing the Web and using the app. Addressing the two challenges, we design and implement a tool to automatically search an Android app for pages with browser components, and then retrieve various ``clean'' information of the embedded Web browsing for later analysis.

\begin{figure}[t!]
    \centering
    \includegraphics[width=0.18\textwidth]{figures/example}
    \caption{An example of embedded Web page. The part in the dashed line box is an embedded page.}~\label{fig:example}
    \vspace{-2em}
\end{figure}

We experiment with a subset of 277 apps that contain an embedded Web page, in the top 500 most downloaded apps on Wandoujia~\cite{wandoujia}. We find 509 instances of embedded Web pages in the 277 apps, indicating that some apps embed multiple Web pages.
The key findings of our measurement study are:
\begin{itemize}
    \item{Embedded Web pages are smaller and have fewer resources than normal mobile Web pages. The median number and size of resources are 10 and 242KB for embedded Web pages compared with 78 objects and 1270.1KB for traditional mobile Web pages.}
    \item{Loading the same embedded Web pages from within the app is faster compared with loading the same Web page from the browser. In the median case, WebView is 1.4x faster than Chrome.}
    %\item{The page load time of embedded Web browsing (829ms in the median case) is much shorter than that of conventional mobile Web browsing via browsers (16436ms in the median case).}
    \item{Browser cache is not fully utilized in embedded Web browsing. In the median case, 90\% of resources can be loaded from the browser cache when the same page is revisited right after the first visit. However, browser cache is almost never used.%Misusing the \texttt{clearCache()} method is a special case for embedded browser components.
        }
\end{itemize}

%Based on our findings, we draw some implications that could help app developers to improve the performance of embedded Web browsing.
In this work, we take the first step towards studying embedded browsing. Broadly, the tools we developed as part of this work can be used to extensively study embedded browsing and its future evolutions.
% design optimizations that are specifically targeted towards improving their performance.

%The remainder of this paper is organized as follows. Section~\ref{sec:background} shows some background knowledge of the browser component \texttt{WebView} provided by Android. Section~\ref{sec:prevalence} reveals to what extend embedded Web browsing is in current mobile apps. Section~\ref{sec:methodology} describes the methodology of our measurement study, including the data set, experiment setup and research questions. Section~\ref{sec:characteristics} and \ref{sec:performance} present the characteristics of embedded Web pages and performance of embedded Web browsing, respectively. %Section~\ref{sec:implication} draws implications based on the findings.
%Section~\ref{sec:related} surveys related work, and Section~\ref{sec:conclusion} concludes the paper.
